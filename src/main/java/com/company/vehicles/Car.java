package com.company.vehicles;

import com.company.details.Engine;
import com.company.entites.Person;
import com.company.professions.Driver;

public class Car {
    private String CarBrand;
    private String CarClass;
    private int wight;
    Engine motor = new Engine();
    Driver driver = new Driver();


    public String getCarClass() {
        return CarClass;
    }

    public void setCarClass(String carClass) {
        CarClass = carClass;
    }

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String carBrand) {
        CarBrand = carBrand;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public void start() {

        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Зупиняємося");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }


    public String toString() {
    motor.setManufacturer("Japan");
    motor.setPower(500);
    driver.setDrivingExperience(5);

        return "Клас авто : " + CarClass + "\n" + "Назва авто : " + CarBrand + "\n" + "Вага авто : " + wight + "\n"  +
                "Виробник мотору : " + motor.getManufacturer() + "\n" +
                "Сила мотору : " + motor.getPower() + "\n" + "Досвід водія : " + driver.getDrivingExperience() + " Років";
    }

}
